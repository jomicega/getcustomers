import React, { Component } from "react";
import "../assets/styles/components/table.scss";
import { connect } from "react-redux";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Paper,
} from "@material-ui/core";

const columns = [
  { id: "id", label: "id", minWidth: 100 },
  { id: "name", label: "Nombre", minWidth: 170 },
  { id: "email", label: "Email", minWidth: 170 },
];

class TableApp extends Component {
  render() {
    const { customers, darkColor } = this.props;
    return (
      <>
        <Typography
          className={
            darkColor
              ? ".title_dark_background_color"
              : ".title_ligth_background_color "
          }
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Clientes
        </Typography>
        <Paper className="root" elevation={4}>
          {customers.length > 0 ? (
            <>
              <TableContainer className="container">
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ minWidth: column.minWidth }}
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  {customers.map((row) => {
                    return (
                      <TableBody>
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.id}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      </TableBody>
                    );
                  })}
                </Table>
              </TableContainer>
            </>
          ) : (
            <div className="message_container">No hay datos para mostrar</div>
          )}
        </Paper>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps, null)(TableApp);
