import React, { Component } from "react";
import { connect } from "react-redux";
import { IconButton, Tooltip } from "@material-ui/core";
import { changeColor } from "../../redux/actions";
import InvertColorsIcon from "@material-ui/icons/InvertColors";

class ButtonColorApp extends Component {
  render() {
    const { darkColor } = this.props;
    const handleChangeColor = () => {
      this.props.changeColor(!darkColor);
    };
    return (
      <Tooltip title="Cambiar fondo de título">
        <IconButton
          variant="contained"
          color="primary"
          onClick={handleChangeColor}
        >
          <InvertColorsIcon />
        </IconButton>
      </Tooltip>
    );
  }
}

const mapDispatchToProps = {
  changeColor,
};

const mapStateToProps = (state) => {
  return state;
};
export default connect(mapStateToProps, mapDispatchToProps)(ButtonColorApp);
