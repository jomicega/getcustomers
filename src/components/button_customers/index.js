import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "@material-ui/core";
import { fetchCustomers } from "../../redux/actions";

class ButtonColorApp extends Component {
  render() {
    const handleFetchCustomer = () => {
      this.props.fetchCustomers();
    };
    return (
      <Button variant="contained" color="primary" onClick={handleFetchCustomer}>
        Obtener clientes
      </Button>
    );
  }
}

const mapDispatchToProps = {
  fetchCustomers,
};

export default connect(null, mapDispatchToProps)(ButtonColorApp);
