import React, { Component } from "react";
import "../assets/styles/components/container.scss";
import ButtonApp from "../button_customers";
import ButtonColor from "../button_color";
import TableApp from "../table";
import { Grid } from "@material-ui/core";

class Container extends Component {
  render() {
    return (
      <div container className="general_container">
        <Grid className="button_container">
          <ButtonApp />
        </Grid>
        <Grid className="table_container">
          <TableApp />
        </Grid>
        <Grid className="button_container">
          <ButtonColor />
        </Grid>
      </div>
    );
  }
}

export default Container;
