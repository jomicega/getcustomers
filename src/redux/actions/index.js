export const initialApp = (payload) => ({
  type: "INITIAL_APP",
  payload,
});

export const changeColor = (payload) => ({
  type: "CHANGE_COLOR",
  payload,
});

export const successGetCustomers = (payload) => ({
  type: "SUCCESS_GET_CUSTOMERS",
  ...payload,
});

const token = "nL6p7nrqP2ivDEKY7dtSEECsN0a7ehywZMaE";

export const fetchCustomers = (payload) => {
  return (dispatch) => {
    fetch(
      `https://gorest.co.in/public-api/users?_format=json&access-token=${token}`
    )
      .then((response) => response.json())
      .then((result) => dispatch(successGetCustomers(result)));
  };
};
