const reducer = (state, action) => {
  switch (action.type) {
    case "SUCCESS_GET_CUSTOMERS":
      const { data, code, meta } = action;
      if (code === 200) {
        return {
          ...state,
          customers: data,
          metaData: meta,
        };
      }
      break;
    case "CHANGE_COLOR":
      return {
        ...state,
        darkColor: action.payload,
      };
    default:
      return {
        ...state,
        darkColor: false,
        customers: [],
        metaData: {
          pagination: {
            total: 0,
            page: 0,
            limit: 0,
          },
        },
      };
  }
};

export default reducer;
