import React, { useEffect } from "react";
import "./components/assets/styles/components/App.scss";
import { connect } from "react-redux";
import { initialApp } from "./redux/actions";
import ContainerApp from "./components/container";

function App(props) {
  useEffect(() => {
    props.initialApp();
  });
  return (
    <div className="App">
      <ContainerApp />
    </div>
  );
}

const mapDispatchToProps = {
  initialApp,
};

export default connect(null, mapDispatchToProps)(App);
